---
stages:
  - lint
  - prep simulation environment
  - oob-mgmt bringup
  - network bringup
  - test the simulation
  - prepare for snapshot
  - create disk images
  - compress disk images
  - copy disk images
  - create topology definition
  - cleanup the simulation
  - trigger downstream pipelines

lint:
  stage: lint
  script:
    - bash ./ci-common/linter.sh

prep:
  stage: prep simulation environment
  script:
    - bash ./ci-common/prep.sh
  only:
    refs:
      - /^dev.*/
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

build-oob-mgmt:
  stage: oob-mgmt bringup
  script:
    - bash ./ci-common/bring-up-oob-mgmt.sh
  artifacts:
    expire_in: 1 week
    untracked: true
    paths:
      - simulation_$CI_PROJECT_NAME/
  only:
    refs:
      - /^dev.*/
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

build-network:
  stage: network bringup
  script:
    - bash ./ci-common/bring-up-network.sh
  artifacts:
    expire_in: 1 week
    untracked: true
    paths:
      - simulation_$CI_PROJECT_NAME/
  only:
    refs:
      - /^dev.*/
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-oob-mgmt

provision-netq:
  stage: network bringup
  script:
    - bash ./ci-common/provision-opta.sh
  only:
    refs:
      - /^dev.*/
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-oob-mgmt

test-from-oob-server:
  stage: test the simulation
  script:
    - bash ./ci-common/test-sim-outside-oob.sh
  only:
    refs:
      - /^dev.*/
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-network

test-from-netq-ts:
  stage: test the simulation
  script:
    - bash ./ci-common/test-sim-outside-netq.sh
  only:
    refs:
      - /^dev.*/
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-network


prepare-for-snapshots:
  stage: prepare for snapshot
  script:
    - bash ./ci-common/snapshot-prep.sh
  only:
    refs:
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-network


create-snapshots:
  stage: create disk images
  script:
    - bash ./ci-common/snapshots.sh
  only:
    refs:
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

compress-images:
  stage: compress disk images
  script:
    - bash ./ci-common/compress-images.sh
  only:
    refs:
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

transfer-images:
  stage: copy disk images
  script:
    - bash ./ci-common/transfer-images.sh
  only:
    refs:
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

destroy-sim:
  stage: copy disk images
  script:
    - bash ./ci-common/cleanup.sh
  only:
    refs:
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-network

create-topology:
  stage: create topology definition
  script:
    - python3 ./ci-common/create-air-topology.py
  only:
    refs:
      - /^master-air$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

cleanup:
  stage: cleanup the simulation
  script:
    - bash ./ci-common/cleanup.sh
  only:
    refs:
      - /^dev.*/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml
  dependencies:
    - build-network

downstream:
  stage: trigger downstream pipelines
  script:
    - bash ./ci-common/downstream-repos.sh
  only:
    refs:
      - /^master$/
    changes:
      - ci-common/*
      - simulation/**/*
      - tests/*
      - .gitlab-ci.yml

